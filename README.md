**Projet Scientific Methodology and Performance Evalution**
*SID-LAKHDAR Riyane COUTAUD Ulysse*

# Performance Evaluation of Memory Allocators for Multi-Threaded Applications.

The point of this project is to evaluate the performance of memory allocators for multi-threaded processes, and to propose a model to be able to forecast the performance of the allocator in givent conditions.<

The work is

1. Motivations for the work
2. Presentation of the previous work we are working on top of
3. Proposition of a model fitting the experimental data set
4. Evaluating the correctness of the proposed model

The work is presented in a [written report](https://gitlab.com/coutaudu/smpe-project/blob/master/report/TER.pdf)