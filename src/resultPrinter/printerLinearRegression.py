import matplotlib.pyplot as plt
import sys
import math
from util import *



# ---------------------------------------
# Auxiliary methods
# ---------------------------------------
def parseRowData(file):
	rowDataX = []
	rowDataY = []
	while not isEndOfFile(file):
		dataLine = nextDataLine(file)
		print dataLine
		print "\n\n\n--------------------------"
		for i in range(1, len(dataLine)):
			rowDataY.append(float(dataLine[i]))
			rowDataX.append(float(dataLine[0]))
	return (rowDataX, rowDataY)


def linearRegression(rowDataX, rowDataY):
	n	= len(rowDataX)
	xMean	= 0
	yMean	= 0
	for i in xrange(n):
		xMean += rowDataX[i]
		yMean += rowDataY[i]
	xMean /= n
	yMean /= n

	betaNominator	= 0
	betaDenominator	= 0
	for i in xrange(n):
		betaNominator	+= (rowDataX[i] - xMean)*(rowDataY[i] - yMean)
		betaDenominator	+= (rowDataX[i] - xMean)*(rowDataX[i] - xMean)
	beta = betaNominator / betaDenominator
	return (yMean - beta*xMean, beta)


def linearError(rowDataX, rowDataY, alpha, beta):
	res = []
	for i in xrange(len(rowDataX)):
		res.append(math.fabs(rowDataY[i] - alpha - beta*rowDataX[i]))
	return res


def plotPointCloud(rowDataX, rowDataY):
	plt.plot(rowDataX, rowDataY, '*', label="Row experimental data", color='green')


def plotLinearRegression(rowDataX, alpha, beta):
	lineY = [(alpha + rowDataX[i] * beta) for i in xrange(len(rowDataX))]
	plt.plot(rowDataX, lineY, '-', label="Optimal linear regression", color='red')


def plotLinearError(owDataX, linearError):
	plt.plot(rowDataX, linearError, '-', label="Least square error", color='blue')


# ---------------------------------------
# Main method
# ---------------------------------------
if __name__ == "__main__":
	rowDataFile		= open(sys.argv[1])
	(rowDataX, rowDataY)	= parseRowData(rowDataFile)
	(alpha, beta)		= linearRegression(rowDataX, rowDataY)
	linearError		= linearError(rowDataX, rowDataY, alpha, beta)

	plt.figure()
	plotPointCloud(rowDataX, rowDataY)
	plotLinearRegression(rowDataX, alpha, beta)
	plt.xlabel("Number of threads")
	plt.ylabel("Throughput (# operation / second)")
	plt.title("Linear regression: y = " + str(beta) + " + x * " + str(alpha))
        plt.grid()
        plt.legend(loc=2)  # Put the legend on the left
	plt.show()


	plt.figure()
	plotLinearError(rowDataX, linearError)
	plt.xlabel("Number of threads")
	plt.ylabel("Least square error")
        plt.grid()
        plt.legend(loc=2)  # Put the legend on the left
	plt.show()

