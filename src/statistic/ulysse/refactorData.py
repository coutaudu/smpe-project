import matplotlib.pyplot as plt
import sys
import math
from util import *



# ---------------------------------------
# Auxiliary methods
# ---------------------------------------
def parseRowData(fileList, outFile):
	while not isEndOfFile(fileList[0]):
		dataLineList = [nextDataLine(fileList[i]) for i in xrange(len(fileList))]
		outFile.write(dataLineList[0][0] + " ")
		for i in xrange(len(dataLineList)):
			outFile.write(dataLineList[i][1] + " " + dataLineList[i][2] + " " + dataLineList[i][3] + " ")
		outFile.write("\n")
	return





# ---------------------------------------
# Main method
# ---------------------------------------
if __name__ == "__main__":
	outFile = open(sys.argv[1], 'w')
	fileList= [open(sys.argv[i], 'r') for i in range(1, len(sys.argv))]
	parseRowData(fileList, outFile)
